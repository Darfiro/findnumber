package findnumber.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import findnumber.Variables;
import findnumber.service.ListToStringConverter;
import lombok.Getter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for storing the results of the search operations
 */
@Entity
public class Response
{
    /**
     * Identifier for a response
     */
    @Id
    @JsonIgnore
    @Getter
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private long responseId;

    /**
     * Code for the result of an operation
     */
    @Getter
    private String code;

    /**
     * Number that was searched
     */
    @JsonIgnore
    @Getter
    private int number;

    /**
     * List of filenames, in which the number was found
     */
    @Convert(converter = ListToStringConverter.class)
    private List<String> filenames;

    /**
     * The error message, that accompanies the result code
     */
    @Getter
    private String error;

    public Response(int number)
    {
        this.number = number;
        this.filenames = new ArrayList<>();
    }

    /**
     * Adds new filename to the list
     * @param name filename to be added
     */
    public void addFilename(String name)
    {
        filenames.add(name);
    }

    /**
     * Getter for the list of filenames, which converts the list to string
     * @return string, where filenames are divided by comma
     */
    public String getFilenames()
    {
        return String.join(",", filenames);
    }

    /**
     * Sets sode and error message
     * @param code enum element, containing code and message
     */
    public void setCode(Variables.Codes code)
    {
        this.code = code.getCode();
        this.error = code.getErrorMessage();
    }
}
