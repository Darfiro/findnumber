package findnumber;

import lombok.Getter;

/**
 * Class for constants
 */
public class Variables
{
    /**
     * Folder for files, in which the search is performed
     */
    public static String filesFolder ="files/";

    /**
     * Codes for the result of search operation
     */
    public static enum Codes
    {
        Result_OK("00.Result.OK", "Число найдено"),
        Result_NotFound("01.Result.NotFound", "Число не найдено"),
        Result_Error("02.Result.Error", "Возникла ошибка при выполнении"); // TODO make several error codes: FileNotFound, ExecutionException etc.

        @Getter
        String code;
        @Getter
        String errorMessage;

        Codes(String code, String error)
        {
            this.code = code;
            this.errorMessage = error;
        }
    }
}
