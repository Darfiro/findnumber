package findnumber.controller;

import findnumber.Variables;
import findnumber.model.Response;
import findnumber.service.DataBaseService;
import findnumber.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Controller for FindNumber operation
 */
@RestController
@RequestMapping(value = "/FindNumber")
public class ResponseController
{
    /**
     * Database service for saving the resuts of the operations
     */
    @Autowired
    private DataBaseService dbs;


    /**
     * Method, that's called when the number is searched
     * @param number number for search
     * @return Response object
     */
    @GetMapping("{number}")
    public ResponseEntity<Response> findNumber(@PathVariable int number)
    {
        Response response = new Response(number);
        boolean error = false;
        List<Callable<Boolean>> tasks = new ArrayList<>();
        int start = 0;
        int finish = 4;
        for (int i = 0; i < 5; i++)
        {
            int finalStart = start;
            int finalFinish = finish;
            Callable<Boolean> callableTask = () -> FileService.searchFiles(number, response, finalStart, finalFinish);
            tasks.add(callableTask);
            start += 4;
            finish += 4;
        }
        ExecutorService executorService = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS,
                                                                new LinkedBlockingQueue<>());
        try
        {
            List<Future<Boolean>> futures = executorService.invokeAll(tasks);
            for(int i = 0; i < futures.size() && !error; i++)
            {
                if (futures.get(i).get())
                    error = true;
            }
        }
        catch (InterruptedException | ExecutionException e)
        {
            error = true;
        }

        if (!error)
        {
            if (!response.getFilenames().equals(""))
                response.setCode(Variables.Codes.Result_OK);
            else
                response.setCode(Variables.Codes.Result_NotFound);
        }
        else
            response.setCode(Variables.Codes.Result_Error);
        dbs.saveResponse(response);
        return ResponseEntity.ok(response);
    }

}
