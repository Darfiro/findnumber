package findnumber.service;

import findnumber.Variables;
import findnumber.model.Response;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

/**
 * Class for file operations
 */
public class FileService
{
    /**
     * Creates 1 GB file with numbers, separated by comma. If destination folder doesn't exist - it will be created. If file already exists it won't be created.
     * @param name file name
     * @param range range for random number generator (numbers will be generated (-range; range))
     * @return true if file is created, false if it already exists
     * @throws IOException
     */
    public static boolean createFile(String name, int range) throws IOException
    {
        File folder = new File(Variables.filesFolder);
        if (!folder.exists())
        {
            folder.mkdir();
        }
        name = Variables.filesFolder + name;
        System.out.println(name);
        File file = new File(name);
        boolean created = false;
        Random random = new Random();
        if (file.createNewFile())
        {
            created = true;
            FileWriter writer = new FileWriter(name);
            int num = random.nextInt(2*range)-range;
            writer.write(num + "");
            while ((double)file.length()/(1024*1024) < 1)
            {
                num = random.nextInt(2*range)-range;
                writer.write("," + num);
            }
            writer.close();
        }
        return created;
    }

    /**
     * Performs a search of a number in files, numbered from start to finish.
     * @param number number for search
     * @param response object of a response, containing names of files, which had the searched number
     * @param start the sequential number of a file identifying the beginning of a range of files
     * @param finish the sequential number of a file identifying the beginning of a range of files
     * @return true if an error occurred
     */
    public static boolean searchFiles(int number, Response response, int start, int finish)
    {
        boolean error = false;
        for (int i = start; i < finish && !error; i++)
        {
            String name = "file" + i + ".txt";
            try
            {
                if (FileService.searchFile(number, name))
                {
                    response.addFilename(name);
                }
            } catch (IOException e)
            {
                error = true;
            }
        }
        return error;
    }

    /**
     * Performes a search of a number in a file
     * @param number the number for search
     * @param name the name of a file
     * @return true if the number was found
     * @throws IOException
     */
    public static boolean searchFile(int number, String name) throws IOException
    {
        boolean found = false;
        name = Variables.filesFolder + name;
        FileInputStream inputStream = new FileInputStream(name);
        Scanner scanner = new Scanner(inputStream, "UTF-8").useDelimiter("\\s*,\\s*");

        while(scanner.hasNextInt() && !found)
        {
            int num = scanner.nextInt();
            if (num == number)
                found = true;
        }
        if (!found)
        {
            scanner.useDelimiter("\\p{javaWhitespace}+");
            if (scanner.hasNext())
            {
                String next = scanner.next().substring(1);
                int num = Integer.parseInt(next);
                if (num == number)
                    found = true;
            }
        }

        inputStream.close();
        scanner.close();
        return found;
    }
}
