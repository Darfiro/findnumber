package findnumber.service;

import findnumber.model.Response;
import findnumber.repository.ResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class for performing database operations
 */
@Service
@Transactional(readOnly = false)
public class DataBaseService
{
    /**
     * Repository for response
     */
    @Autowired
    private ResponseRepository responseRepository;

    /**
     * Saves response object to database
     * @param response object to save
     * @return saved objected
     */
    public Response saveResponse(Response response)
    {
        return responseRepository.save(response);
    }

}
