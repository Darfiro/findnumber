package findnumber.service;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class for converting list of strings into one string and vise versa
 */
@Converter
public class ListToStringConverter implements AttributeConverter<List<String>, String>
{
    /**
     * Converts list of strings into a string
     * @param list list to be converted
     * @return formed string, where the elements of the list are divided by comma
     */
    @Override
    public String convertToDatabaseColumn(List<String> list)
    {
        return String.join(",", list);
    }

    /**
     * Converts string into a list of strings
     * @param string string to be converted
     * @return the list of strings
     */
    @Override
    public List<String> convertToEntityAttribute(String string)
    {
        return new ArrayList<>(Arrays.asList(string.split(",")));
    }

}
