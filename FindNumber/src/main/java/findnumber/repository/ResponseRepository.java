package findnumber.repository;

import findnumber.model.Response;
import org.springframework.data.repository.CrudRepository;

/**
 * Database repository for Response
 */
public interface ResponseRepository extends CrudRepository<Response, Long>
{
}
