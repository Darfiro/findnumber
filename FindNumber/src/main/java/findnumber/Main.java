package findnumber;

import findnumber.service.FileService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;


@SpringBootApplication
public class Main
{

    public static void main(String[] args)
    {
        generateFiles(20);
        SpringApplication.run(Main.class, args);
    }

    /**
     * Generates files with numbers
     * @param fileNumber number of generated files
     */
    private static void generateFiles(int fileNumber)
    {
        for(int i = 0; i < fileNumber; i++)
        {
            try
            {
                FileService.createFile("file" + i + ".txt", (i + 1)*10);
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
